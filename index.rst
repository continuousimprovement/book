.. my_book documentation master file, created by
   sphinx-quickstart on Wed Nov 15 22:08:51 2017.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Write a book with python and sphinx
===================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

This is the first paragraph of text. 
I am writing one sentence in each line. 
I like the idea of one sentence per line. 
What I don't get is should I have a trailing space at the end of my word or not? 



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
